# adneom

TEST TECHNIQUE ADNEOM
Ce test est réalisable en 2h, pour faire quelque chose de correct, mais libre à vous d’aller
plus loin. Vous êtes également entièrement libre d’utiliser ou non symfony, l’idée principale
étant d’évaluer votre niveau de base en PHP ainsi que votre capacité à appliquer les bonnes
pratiques communes à tout langage de programmation (DRY, KISS etc.).
Une attention toute particulière sera apportée à la lisibilité de votre code ainsi qu’à sa
performance d’exécution.
Vous devrez restituer votre travail en communiquant votre dépôt git ou archive zip.
NB: Si vous réalisez le test technique avec Symfony il est inutile de joindre le dossier
“vendor”.
_________________________________________________________________________
Context:
A partir d’un CRM fictif dont la documentation api est disponible à l’adresse
http://conception.website/adneom/api , vous devrez remplir ces 2 objectifs:
1) Déterminer si un utilisateur spécifique est abonné à un produit selon une date
spécifique.
L’algorithme devra par exemple pouvoir déterminer que le 01/10/2018 l’utilisateur
correspondant à l’id #90 ( http://conception.website/adneom/api/users/90 ) était bien abonné
au produit comportant l’id #9.
2) Retourner tous les ids d’utilisateurs abonnés à un produit spécifique pour une date
spécifique.
Le script devra par exemple déterminer que 162 utilisateurs ( ids: #2, ..., #761 ) ont été
abonnés au produit comportant l’id #10 le 01/10/2018.
_________________________________________________________________________
Vous pourrez vérifier le bon fonctionnement de votre code sur cette page:
http://www.conception.website/api/adneom/check.php