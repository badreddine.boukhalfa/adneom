<?php

namespace App\Controller;

use Doctrine\DBAL\Types\ArrayType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ServiceData;

class ManagerDataController extends AbstractController
{
    
    /**
     * @Route("/client/product/Subcriber", name="manager_data")
     */
    public function index(Request $request,ServiceData $check): Response
    {
       
        $user_id=$request->request->get('user_id');
        $product_id=$request->request->get('product_id');
        $date=$request->request->get('date');
        $query=$request->request->get('q');
        $subscriber='';
       
        if(isset($user_id) && isset($product_id)  && isset($date) && $query)
        {
            $subscriber= $check->checkSubscriber($user_id,$product_id,$date);
        }

        return $this->render('manager_data/index.html.twig', [
            'controller_name' => 'ManagerDataController',
            'subscriber'=>$subscriber,
        ]);
    }

    /**
     * @Route("/clients/product/Subcriber", name="get_client_by_product_Subcriber")
     */
    public function getClientByIdProductAndDateSubcriber(Request $request,ServiceData $check) : Response
    {

        $product_id=$request->request->get('product_id');
        $date=$request->request->get('date');
        $query=$request->request->get('q');
       
        return $this->render('manager_data/index.html.twig', [
            'controller_name' => 'ManagerDataController',
            'costumers'=>$check->getClientsSubscribeProduct($product_id,$date,$query),
            'product'=>$product_id,
            'date'=> $date
        ]);
    }   

}
