<?php
namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServiceData
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $client
     * @return array
     */
    function getClientById($client) :array
    {
        if ($client)
        {
            $url='http://conception.website/adneom/api/users/'.$client;
        }
        else
        {
            $url='http://conception.website/adneom/api/users';
        }
        
        $response = $this->client->request('GET',$url);

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();

        return  $content;
    }


     /**
     * @param int $user_id
     * @param int $product_id
     * @param date $date
     * @return string $subscriber
     */
    function checkSubscriber($user_id,$product_id,$date)
    {
        $subscriber='Cet utilisateur '.$user_id.' n\'est pas abonné à ce produit #'.$product_id.' à cette date: '.$date;
        $data=$this->getClientById($user_id);
        foreach ($data['products'] as $key => $value)
        {
            if ($value['id']==$product_id)
            {
                foreach ($value['subscriptionPeriods'] as $key => $value)
                {
                    if($value['start']<=$date && $value['end']>= $date )
                    {
                        $subscriber='Cet utilisateur #'.$user_id.' est abonné à ce produit #'.$product_id.' à cette date: '.$date.'.';
                        return $subscriber;
                    }
                }
            }    
            
        }

        return $subscriber;
    }

    
     /**
     * @param array $data
     * @return array $client
     */
    function getClientAndProduct($data)
    {
        $client=[];

        foreach ($data as $key => $value)
        {
            $client[$value['id']]=$value['products'];
        }    
        return  $client;

    }


     /**
     * @param int $product_id
     * @param date $date
     * @return array $costumer
     */
    function getClientsSubscribeProduct($product_id,$date,$query)
    {
        $costumer=[];
        if(isset($product_id) && isset($date) && isset($query))
        {
            $data= $this->getClientById($client='');
            $client=$this->getClientAndProduct($data);
            $j=0;
            for($i=1;$i<count($client)+1;$i++)
            {
                foreach ($client[$i] as $key => $value)
                {
                    if ($value['id']==$product_id)
                    {   
                        foreach ($value['subscriptionPeriods'] as $key => $value)
                        {
                            if($value['start']<=$date && $value['end']>= $date )
                            {
                                $costumer[$j]=$i;
                                $j++;
                            }
                        }
                    } 
                }
            }
            
            return array_unique($costumer);
        }
        return $costumer;
    }

}